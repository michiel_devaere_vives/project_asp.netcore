﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProEmbV4.Models
{
    public class UsedModels
    {
        public string Key { get; internal set; }
        public string Name { get; internal set; }
        public string Type { get; internal set; }

    }
    public class UsedModelsDataContext
    {
        public IEnumerable<UsedModels> GetUsedModels()
        {
            return new[]
            {
                new UsedModels {
                    Key = "LoraWan",
                    Name = "Draadloze Communicatie",
                    Type = "LoraWan",
                    
                },
                new UsedModels {
                    Key = "TC74",
                    Name = "Temperatuur Meting",
                    Type = "TC74",
                    
                },
                new UsedModels {
                    Key = "Webcam",
                    Name = "Webcam Embedded",
                    Type = "Webcam",
                    
                },
                new UsedModels {
                    Key = "Relais",
                    Name = "Relais Embedded",
                    Type = "Relais",
                    
                },
            };
        }
    }
}
