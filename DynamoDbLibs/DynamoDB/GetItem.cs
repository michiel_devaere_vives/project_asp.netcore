﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.Model;
using DynamoDbLibs.Models;

namespace DynamoDbLibs.DynamoDB
{
    public class GetItem : IGetItem
    {
        private readonly IAmazonDynamoDB _dynamoDbClient;

        public GetItem(IAmazonDynamoDB dynamoDbClient)
        {
            _dynamoDbClient = dynamoDbClient;
        }

        public async Task<DynamoTableItems> GetAllItems()
        {
            var queryRequest = RequestbuilderAllItems();
            var result = await ScanAsync(queryRequest);

            return new DynamoTableItems
            {
                Items = result.Items.Select(Map).ToList()
            };
        }

        private ScanRequest RequestbuilderAllItems()
        {
            return new ScanRequest
            {
                TableName = "tc74Final"
            };
        }

        private async Task<ScanResponse> ScanAsync(ScanRequest request)
        {
            var response = await _dynamoDbClient.ScanAsync(request);

            return response;
        }

        public async Task<DynamoTableItems> GetSomeItems(string dev_id)
        {
            var queryRequest = RequestbuilderSomeItems(dev_id);
            var result = await ScanAsync(queryRequest);

            return new DynamoTableItems
            {
                Items = result.Items.Select(Map).ToList()
            };

        }

        private ScanRequest RequestbuilderSomeItems(string dev_id)
        {

            return new ScanRequest
            {
                TableName = "tc74Final",
                ExpressionAttributeValues = new Dictionary<string, AttributeValue>
                {
                    {
                        ":v_Dev_Id", new AttributeValue {N = dev_id}
                    }
                },
                FilterExpression = "Dev_id = :v_Dev_Id",
                ProjectionExpression = "Dev_id, time, temperature"
            };
        }

        private Item Map(Dictionary<string, AttributeValue> result)
        {
            return new Item
            {
                dev_id = Convert.ToString(result["dev_id"].S),
                time = Convert.ToString(result["time"].S),
                temperature = Convert.ToString(result["temperature"].S)
            };
        }
    }
}