﻿using Microsoft.AspNetCore.Mvc;
using ProEmbV4.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProEmbV4.ViewComponents
{
    [ViewComponent]
    public class UsedModelsViewComponent : ViewComponent
    {
        private readonly UsedModelsDataContext _usedModels;

        public UsedModelsViewComponent(UsedModelsDataContext usedModels)
        {
            _usedModels = usedModels;
        }
        public IViewComponentResult Invoke()
        {
            var usedModels = _usedModels.GetUsedModels();
            return View(usedModels);
        }
    }
}
