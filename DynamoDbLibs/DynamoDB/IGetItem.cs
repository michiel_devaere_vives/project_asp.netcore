﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using DynamoDbLibs.Models;

namespace DynamoDbLibs.DynamoDB
{
    public interface IGetItem
    {
        Task<DynamoTableItems> GetAllItems();
        Task<DynamoTableItems> GetSomeItems(string dev_id);
    }
}
