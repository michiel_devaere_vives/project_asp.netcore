﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Amazon.DynamoDBv2;
using DynamoDbLibs.DynamoDB;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using ProEmbV4.Models;
using MySql.Data.EntityFrameworkCore.Extensions;


namespace ProEmbV4
{
    public class Startup
    {
        public Startup()
        {
               Configuration = new ConfigurationBuilder()
                   .SetBasePath(Directory.GetCurrentDirectory())
                   .AddJsonFile("appsettings.json")
                   .Build(); 
        }

        public IConfigurationRoot Configuration  { get; set; }
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddTransient<UsedModelsDataContext>();
            services.AddTransient <FormattingService>();
            //services.AddDbContext<BlogDataContext>(options =>
            //{              
            //    options.UseMySQL("BlogDataContext");
            //});
            services.AddMvc();

            services.AddDefaultAWSOptions(Configuration.GetAWSOptions());

            Environment.SetEnvironmentVariable("AWS_ACCESS_KEY_ID", Configuration["AWS:AccessKey"]);
            Environment.SetEnvironmentVariable("AWS_SECRET_ACCESS_KEY", Configuration["AWS:SecretKey"]);
            Environment.SetEnvironmentVariable("AWS_REGION", Configuration["AWS:Region"]);

            services.AddAWSService<IAmazonDynamoDB>(); 

            services.AddSingleton<IGetItem, GetItem>();
        }
        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseExceptionHandler("/error.html");

            var configuration = new ConfigurationBuilder().AddEnvironmentVariables().AddJsonFile(env.ContentRootPath + "/config.json")
                .Build();
            // Custom Configuration

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseMvc(routes =>
            {
                routes.MapRoute("Default",
                    "{controller=Home}/{action=Index}/{id?}"
                    );
            });     // Middleware registred
            app.UseFileServer();
            // Looks for pages in the wwwroot folder.
        }
    }
}
