﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ProEmbV4.Models;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace ProEmbV4.Controllers
{
    [Route("webcam")]
    public class WebcamController : Controller
    {
        [HttpGet, Route("webcam")]
        public IActionResult Webcam()
        {
            return View();
        }
    }
}
