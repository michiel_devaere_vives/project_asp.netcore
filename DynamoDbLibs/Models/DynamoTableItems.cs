﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DynamoDbLibs.Models
{
    public class DynamoTableItems
    {
        public IEnumerable<Item> Items { get; set; }
    }

    public class Item
    {
        public string dev_id { get; set; }
        public string time { get; set; }
        public string temperature { get; set; }
    }
}
