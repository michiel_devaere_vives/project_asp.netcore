﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using System.Web.Helpers;
using DynamoDbLibs.DynamoDB;
using DynamoDbLibs.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Razor.TagHelpers;

namespace ProEmbV4.Controllers
{
    [Produces("application/json")]
    [Route("api/DynamoDB")]
    public class DynamoDBController : Controller
    {
        private readonly IGetItem _getItem;

        public DynamoDBController(IGetItem getItem)
        {
            _getItem = getItem;
        }

        [Route("getSomeItems")]
        public async Task<IActionResult> GetSomeItems([FromQuery] string dev_id)
        {
            var response = await _getItem.GetSomeItems(dev_id);


            return Ok(response);
        }

        [Route("getAllItems")]
        public async Task<IActionResult> GetAllItems()
        {
            var response =  await _getItem.GetAllItems();
            
            //return Ok(response);
            return View(response);
        }
    }
}